# termux

## Wayland

- Executing startvnc will initiate Weston (Wayland).

## Phantom

- To disable Phantom in Android 12L and newer versions. navigate to `Settings` >> `Developer Options` >> `Feature Flags`. and disable `settings_enable_monitor_phantom_procs`. It's important to note that some manufacturers. such as Samsung. Xiaomi. and others. may hide this `settings_enable_monitor_phantom_procs` option within the Feature Flags. making it necessary to use ADB for disabling it. To proceed. ensure ADB is installed on your computer. and execute the following command: `adb shell /system/bin/settings put global settings_enable_monitor_phantom_procs false`

- If you're using Android 12. it's necessary to upgrade to Android 12L. If your device no longer receives Android updates. consider downgrading to Android 11. or use a custom ROM with Android 12L. or a GSI with Android 12L version.

## Steps to install

- pkg update && pkg upgrade

- pkg install git --no-install-recommends

- git clone https://gitlab.com/manoel-linux1/termux.git

- cd termux

- chmod a+x `termux.sh`

- `./termux.sh`

## GPU

- GPU acceleration is not working.

## SSH

- ssh username@192.168.0.00 -p8022 ## Use this command to connect Termux to the computer using SSH.
