clear

pkg update && pkg upgrade

pkg install x11-repo tur-repo glibc-repo --no-install-recommends

pkg update && pkg upgrade

pkg install coreutils openssh mesa mesa-vulkan-icd-swrast vulkan-volk vulkan-loader vulkan-extension-layer vulkan-tools ffmpeg wget unzip nano weston chromium --no-install-recommends

passwd

pkg clean && pkg autoclean && apt autoremove

cd $HOME

rm -rf weston.ini

rm -rf ../usr/bin/startchromium

rm -rf ../usr/bin/startvnc

rm -rf ../usr/bin/stopvnc

rm -rf ../usr/bin/mini-opti

rm -rf ../usr/bin/opti-termux

rm -rf ../usr/bin/termux-tweaks

cp -r termux/weston.ini weston.ini

cp -r termux/startchromium ../usr/bin/

cp -r termux/startvnc ../usr/bin/

cp -r termux/stopvnc ../usr/bin/

cp -r termux/mini-opti ../usr/bin/

cp -r termux/opti-termux ../usr/bin/

cp -r termux/termux-tweaks ../usr/bin/

chmod +x ../usr/bin/startchromium

chmod +x ../usr/bin/startvnc

chmod +x ../usr/bin/stopvnc

chmod +x ../usr/bin/mini-opti

chmod +x ../usr/bin/opti-termux

chmod +x ../usr/bin/termux-tweaks